﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RetoEmail.SMTPService
{
    public interface ISMTPParameters
    {
  
        string From { get; set; }
        string User { get; set; }
        string Password { get; set; }
        string Server { get; set; }
        int Port { get; set; }
        bool UseSSL { get; set; }
    }
}
