﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace RetoEmail.SMTPService
{
    public class SMTPService : ISMTPService
    {
        private SmtpClient _smtp_client;
        private readonly ISMTPParameters _ISMTPParameters;
       
        public SMTPService(ISMTPParameters ISMTPParameters)
        {
            _ISMTPParameters = ISMTPParameters;
            _smtp_client = new SmtpClient(_ISMTPParameters.Server);
            _smtp_client.Port = _ISMTPParameters.Port;           
            _smtp_client.EnableSsl = _ISMTPParameters.UseSSL;

            _smtp_client.UseDefaultCredentials = false;
            _smtp_client.Credentials = new NetworkCredential(_ISMTPParameters.User, _ISMTPParameters.Password);


        }
        public void SendMail(string subject, string body, List<string> Tos)
        { 
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(_ISMTPParameters.From);
                foreach (var to in Tos)
                {
                    mailMessage.To.Add(to);
                }

                mailMessage.Body = body;
                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml = true;
                _smtp_client.Send(mailMessage);               
            }
            catch (Exception ep)
            {
                throw ep;
            }

        }
    }
}
