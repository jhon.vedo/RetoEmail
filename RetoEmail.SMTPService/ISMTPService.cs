﻿using System.Collections.Generic;

namespace RetoEmail.SMTPService
{
   
    public interface ISMTPService
    {
        void SendMail(string subject,string body, List<string> Tos);
    }
}
