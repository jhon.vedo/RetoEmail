﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RetoEmail.CosmosDBService
{
    public interface ICosmosBaseModel
    {
         string Id { get; set; }
        DateTime CreationDate { get; set; }
    }
    public class CosmosBaseModel : ICosmosBaseModel
    {
        public CosmosBaseModel()
        {
            //  Id = Guid.NewGuid().ToString();
            Id = Guid.NewGuid().ToString().ToCharArray().Sum(x => x).ToString();
            CreationDate = DateTime.Now;
        }
        [JsonProperty("id")]
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }

    }
}
