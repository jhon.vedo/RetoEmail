﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RetoEmail.CosmosDBService
{
    public interface ICosmosParameters
    {
        string DataBaseName { get; set; }
        string CollectionName { get; set; }
        string EndPoint { get; set; }
        string PrimaryKey { get; set; }
    }
}
