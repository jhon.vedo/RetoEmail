﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RetoEmail.CosmosDBService
{
    public interface ICosmosService<T> where T : CosmosBaseModel
    {
        Task InsertAsync(T entity);
        IEnumerable<T> Get();
        T Get(string Id);
        Task UpdateAsync(T entity);
        Task DeleteAsync(string Id);

    }
}
