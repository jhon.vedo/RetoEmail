﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RetoEmail.CosmosDBService
{
    public class CosmosService<T> : ICosmosService<T> where T:  CosmosBaseModel
    {
        //string EndpointUrl = "https://retoemailcosmosdb.documents.azure.com:443/";
        //string PrimaryKey = "9rNB8z0AdyZ6Soh5Fpk1YkkDpYQSRIrYAWOfDqEapM58m5V1zaXSPW6mYjyqcFk2e998w6m45wWkWIo257QapQ==";
        //string database = "Test";
        //string collectionname = "col_test";
        private readonly ICosmosParameters _parameters;
        private DocumentClient client;
        public CosmosService(ICosmosParameters parameters)
        {
            _parameters = parameters;
             client = new DocumentClient(new Uri(_parameters.EndPoint), _parameters.PrimaryKey);           

            Database databaseInfo = new Database { Id = _parameters.DataBaseName };
            client.CreateDatabaseIfNotExistsAsync(databaseInfo).Wait();

            DocumentCollection collectionInfo = new DocumentCollection();
            collectionInfo.Id = _parameters.CollectionName;
           // collectionInfo.PartitionKey.Paths.Add("/ID");
            collectionInfo.IndexingPolicy = new IndexingPolicy(new RangeIndex(DataType.String) { Precision = -1 });

     
            client.CreateDocumentCollectionIfNotExistsAsync(
                UriFactory.CreateDatabaseUri(databaseInfo.Id),
                collectionInfo,
                new RequestOptions { OfferThroughput = 400 }).Wait();
        }

      
        public async Task InsertAsync(T entity)
        {
            await this.CreateDocumentIfNotExists(_parameters.DataBaseName, _parameters.CollectionName, entity);

        }

        public IEnumerable<T> Get()
        {
            IEnumerable<T> familyQuery = client.CreateDocumentQuery<T>(UriFactory.CreateDocumentCollectionUri(_parameters.DataBaseName, _parameters.CollectionName)).ToList();
            return familyQuery;

        }

        public T Get(string Id)
        {
            var familyQuery = client.CreateDocumentQuery<T>(UriFactory.CreateDocumentCollectionUri(_parameters.DataBaseName, _parameters.CollectionName)).ToList().Where(X => X.Id.Equals(Id)).FirstOrDefault();
            return familyQuery;

        }

        public async Task UpdateAsync(T entity)
        {
            await client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(_parameters.DataBaseName, _parameters.CollectionName, entity.Id), entity);
        }

        public async Task DeleteAsync(string Id)
        {
            await client.DeleteDocumentAsync(UriFactory.CreateDocumentUri(_parameters.DataBaseName, _parameters.CollectionName, Id));
        }


        private async Task CreateDocumentIfNotExists(string databaseName, string collectionName, T entity)
        {
            try
            {
                await this.client.ReadDocumentAsync(UriFactory.CreateDocumentUri(databaseName, collectionName, entity.Id));
                
            }
            catch (DocumentClientException de)
            {
                if (de.StatusCode == HttpStatusCode.NotFound)
                {
                    await this.client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), entity);
                   
                }
                else
                {
                    throw;
                }
            }
        }


    }

}
