﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RetoEmail.Identity
{
    public class TokenRequest
    {
        [Required]
        [JsonProperty("clientSecret")]
        public string ClientSecret { get; set; }


        //[Required]
        //[JsonProperty("password")]
        //public string Password { get; set; }
    }
}
