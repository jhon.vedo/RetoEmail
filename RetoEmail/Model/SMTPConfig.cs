﻿using Newtonsoft.Json;
using RetoEmail.SMTPService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RetoEmail.Model
{
    [JsonObject("smtpConfig")]
    public class SMTPConfig : ISMTPParameters
    {
        [JsonProperty("from")]
        public string From { get; set; }
        [JsonProperty("user")]
        public string User { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("server")]
        public string Server { get; set; }
        [JsonProperty("port")]
        public int Port { get; set; }
        [JsonProperty("useSSL")]
        public bool UseSSL { get; set; }
    }
}
