﻿using RetoEmail.CosmosDBService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RetoEmail.Model
{
    public class Plantilla: CosmosBaseModel
    {
        public string Body { get; set; }
        public string Titulo { get; set; }
        public List<string> Tags { get; set; }
    }
}
