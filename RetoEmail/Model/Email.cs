﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RetoEmail.Model
{
    public class Email
    {
        [Required]
        public string  Subject { get; set; }
        [Required]
        public string Template { get; set; }
        [Required]
        public List<string> Tos { get; set; }
        public IDictionary<string,string> Params { get; set; }
    }
}
