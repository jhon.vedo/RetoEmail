﻿using Newtonsoft.Json;
using RetoEmail.CosmosDBService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RetoEmail.Model
{
    [JsonObject("cosmosConfig")]
    public class CosmosConfig : ICosmosParameters
    {
        [JsonProperty("dataBaseName")]
        public string DataBaseName { get; set; }
        [JsonProperty("collectionName")]
        public string CollectionName { get; set; }
        [JsonProperty("endPoint")]
        public string EndPoint { get; set; }
        [JsonProperty("primaryKey")]
        public string PrimaryKey { get; set; }
    }
}
