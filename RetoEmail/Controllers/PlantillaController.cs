﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetoEmail.CosmosDBService;
using RetoEmail.Model;

namespace RetoEmail.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PlantillaController : ControllerBase
    {
        private readonly ICosmosService<Plantilla> _cosmos;

        public PlantillaController(ICosmosService<Plantilla> cosmos)
        {
            _cosmos = cosmos;
        }


        [HttpGet]        
        public IActionResult Get()
        {
            var plantillas = _cosmos.Get();
            return Ok(plantillas);
        }

        // GET: api/Plantilla/5
        [HttpGet]
        [Route("{id}")]
        public IActionResult Get([FromRoute]string id)
        {
            var plantilla = _cosmos.Get(id);
            return Ok(plantilla);
            
        }

        // POST: api/Plantilla
        [HttpPost]

        public async Task<IActionResult> PostAsync([FromBody] Plantilla body)
        {
             await _cosmos.InsertAsync(body);
            return Ok();           
        }

      
        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> PutAsync([FromRoute]string id, [FromBody] Plantilla body)
        {
            await _cosmos.UpdateAsync(body);
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute]string id)
        {
            await _cosmos.DeleteAsync(id);
            return Ok();
        }
    }
}
