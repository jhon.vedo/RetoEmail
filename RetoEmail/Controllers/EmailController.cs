﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetoEmail.CosmosDBService;
using RetoEmail.Model;
using RetoEmail.SMTPService;

namespace RetoEmail.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {

        private readonly ISMTPService _service;
        private readonly ICosmosService<Plantilla> _cosmos;
        public EmailController(ISMTPService service, ICosmosService<Plantilla> cosmos)
        {
            _service = service;
            _cosmos = cosmos;
        }
        

        [HttpPost]
        public IActionResult Post([FromBody] Email value)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var template = _cosmos.Get(value.Template);

            if(template == null)
            {
                return BadRequest("Template no encontrado");
            }

            if(value.Params != null)
            {
                foreach (var param in value.Params)
                {
                    template.Body = template.Body.Replace(param.Key,param.Value);
                }
            }
             _service.SendMail(value.Subject, template.Body, value.Tos);

            return Ok();
        }







    }
}
